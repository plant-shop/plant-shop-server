const express = require('express');
const router = express.Router();
const moistureTypesController = require('../controllers/moistureTypes.controller');

router.get('/', moistureTypesController.getAll);

module.exports = router;
