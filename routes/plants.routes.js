const express = require('express');
const router = express.Router();
const plantsController = require('../controllers/plants.controller');
const multer = require('multer');
const path = require('path');


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname, '../uploads'))
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
})
const upload = multer({storage: storage});

router.get('/', plantsController.getAll);
router.get('/:id', plantsController.getPlant);
router.get('/raw/:id', plantsController.getRawPlant);
router.delete('/:id', plantsController.deletePlant);
router.post('/', upload.fields([{ name: 'image1', maxCount: 1 },
    { name: 'image2', maxCount: 1 },
    { name: 'image3', maxCount: 1 }]),
    plantsController.add);
router.post('/:id', upload.fields([{ name: 'image1', maxCount: 1 },
    { name: 'image2', maxCount: 1 },
    { name: 'image3', maxCount: 1 }]),
    plantsController.update);


module.exports = router;
