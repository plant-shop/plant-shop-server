const express = require('express');
const router = express.Router();
const plantCategoriesController = require('../controllers/plantCategories.controller');

router.get('/', plantCategoriesController.getAll);

module.exports = router;
