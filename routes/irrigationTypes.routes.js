const express = require('express');
const router = express.Router();
const irrigationTypesController = require('../controllers/irrigationTypes.controller');

router.get('/', irrigationTypesController.getAll);

module.exports = router;
