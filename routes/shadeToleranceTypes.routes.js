const express = require('express');
const router = express.Router();
const shadeToleranceTypesController = require('../controllers/shadeToleranceTypes.controller');

router.get('/', shadeToleranceTypesController.getAll);

module.exports = router;
