const express = require('express')
const pool = require('./config/db.config');
const plantsRoutes = require('./routes/plants.routes');
const irrigationTypesRoutes = require('./routes/irrigationTypes.routes');
const moistureTypesRoutes = require('./routes/moistureTypes.routes');
const plantCategoriesRoutes = require('./routes/plantCategories.routes');
const shadeToleranceTypesRoutes = require('./routes/shadeToleranceTypes.routes');


const app = express();
const port = 3000;

app.use(function (req, res, next) {
    const origins = [
        'http://localhost:3001',
    ];

    if(req.headers.origin) {
        origins.forEach((origin) => {
            if(req.headers.origin.indexOf(origin) > -1){
                res.header('Access-Control-Allow-Origin', req.headers.origin);
            }
        });
    }

    // res.header('Access-Control-Allow-Origin', '*');
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/uploads'));

app.use('/plants', plantsRoutes);
app.use('/irrigation-types', irrigationTypesRoutes);
app.use('/moisture-types', moistureTypesRoutes);
app.use('/plant-categories', plantCategoriesRoutes);
app.use('/shade-tolerance-types', shadeToleranceTypesRoutes);

app.get('/', (req, res) => {
    res.send('Hello World!')
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
