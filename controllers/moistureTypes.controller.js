const pool = require('../config/db.config');

const getAll = (req, res, next) => {
    pool.query('SELECT * FROM moisture_type;', (err, moisture_types) => {
        if(err) {
            res.status(404).send(err);
        }
        res.send(moisture_types);
    });
};

module.exports = {getAll};
