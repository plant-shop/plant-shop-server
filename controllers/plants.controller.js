const pool = require('../config/db.config');

const getFilePath = (files, host) => {
    if(!files) return '';
    return `${host}/${files[0].originalname}`;
}

const getAll = (req, res, next) => {
    pool.query('SELECT * FROM plant;', (err, plants) => {
        if(err) {
            res.status(404).send(err);
        }
        res.send(plants);
    });
};

const getPlant = (req, res, next) => {
    const id = req.params.id;
    pool.query('SELECT * from plant WHERE plant.id = ?;', [id], (err, plants) => {
        if(err) {
            res.send(err);
        }
        if(plants.length === 0) {
            res.send('123');
        }
        res.send(plants[0]);
    });
};

const getRawPlant = (req, res, next) => {
    const id = req.params.id;
    pool.query('SELECT * FROM plant WHERE plant.id = ?;', id, (err, plants) => {
        if(err) {
            res.send(err);
        }
        if(plants.length === 0) {
            res.send('123');
        }
        res.send(plants[0]);
    });
}

const deletePlant = (req, res, next) => {
    const id = req.params.id;
    pool.query('DELETE FROM PLANT WHERE id = ?', id, (err, result) => {
       if(err) {
           return err;
       }
       return res.send(result);
    });
};

const add = (req, res, next) => {
    const plant = {...req.body};
    const hostUrl = req.protocol + '://' + req.get('host');
    plant['image1'] = getFilePath(req.files['image1'], hostUrl);
    plant['image2'] = getFilePath(req.files['image2'], hostUrl);
    plant['image3'] = getFilePath(req.files['image3'], hostUrl);
    pool.query('INSERT INTO plant SET ?;', plant, (error, result) => {
        if (error) {
            res.json(error);
        } else {
            plant.id = result.insertId;
            res.json(plant);
        }
    });
}

const update = (req, res, next) => {
    const id = req.params.id;
    const plant = {...req.body};
    const hostUrl = req.protocol + '://' + req.get('host');
    plant['image1'] = req.files['image1'] ? getFilePath(req.files['image1'], hostUrl) : plant['image1'];
    plant['image2'] = req.files['image2'] ? getFilePath(req.files['image2'], hostUrl) : plant['image2'];
    plant['image3'] = req.files['image3'] ? getFilePath(req.files['image3'], hostUrl) : plant['image3'];
    pool.query('UPDATE plant SET ? WHERE id = ?;', [plant, id], (error, result) => {
        if (error) throw error;
        res.json(plant);
    });
}


module.exports = {getAll, getPlant, deletePlant, add, getRawPlant, update};
