const pool = require('../config/db.config');

const getAll = (req, res, next) => {
    pool.query('SELECT * FROM shade_tolerance_type;', (err, shade_tolerance_types) => {
        if(err) {
            res.status(404).send(err);
        }
        res.send(shade_tolerance_types);
    });
};

module.exports = {getAll};
