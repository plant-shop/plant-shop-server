const pool = require('../config/db.config');

const getAll = (req, res, next) => {
    pool.query('SELECT * FROM plant_category;', (err, plant_categories) => {
        if(err) {
            res.status(404).send(err);
        }
        res.send(plant_categories);
    });
};

module.exports = {getAll};
