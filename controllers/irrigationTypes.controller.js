const pool = require('../config/db.config');

const getAll = (req, res, next) => {
    pool.query('SELECT * FROM irrigation_type;', (err, irrigation_types) => {
        if(err) {
            res.status(404).send(err);
        }
        res.send(irrigation_types);
    });
};

module.exports = {getAll};
